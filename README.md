# Nymbl
Want to launch or incorporate your very own original on-demand products site & service anywhere in the world? 

The Nymbl platform was developed to support campaign / crowdfunding style (e.g. Teespring, Viralstyle, etc.), artist shops (e.g. Redbubble, Society6, etc.), gift / other sites (e.g. Cafepress, Zazzle, etc.) or promotional / incentives products (4Imprint, Hitpromo, etc.). 

In exchange for making the software open source we simply ask that you take advantage of our Product Image Processing and Production & Fulfillment services.

Nymbl is sponsored by Printico, Inc. and is based on the capabilities of http://www.printi.co

# Forum
Our forum for all questions, ideas, discussion, you name it: https://groups.google.com/forum/#!forum/nymbl

# License
Our software is 100 percent GPL, and if yours too is 100 percent GPL, then you never have to pay us for the licenses. In all other instances, or if you are a printing / production operator, you are better served by our commercial license. Please contact us at info at benymbl dot com for details.

# API
We offer an API for product mockups and store creation which allows you to build your own UI and apps. Contact us for details and pricing at info at benymbl dot com

# Notice
We are currently cleaning up the code but wanted to make it available in a timely fashion. Updates coming soon.

We believe we have create something useful for the world, we are going to continue to develop the code for the foreseeable future, and we are accepting outside contributions.

# Developer Guide
If you want to contribute to the project, or even just tinker on your own, this guide explains how to get the source and work with it. https://github.com/Nymbl/Nymbl/wiki/Developer-Guide

# Installation Instructions 
Coming soon at https://github.com/Nymbl/Nymbl/wiki/Installation-Instructions. We are offering **free installation assistance for a limited time**.

# Overview

The Nymbl platform consists of:

##### 1. E-commerce Engine, Database and UI:
The code and database in this repository to be installed on your web server (PHP and MySQL).
##### 2. Nymbl Image Processing Servers: 
This is where an uploaded image is processed to create photorealistic product mockups. We offer this service in the cloud on a usage basis. Nymbl can program virtually any product for you. Please contact us for details and usage-based pricing. 
##### 3. Nymbl Product Production & Fulfillment:
We are a network orchestrator of printing / production and fulfillment providers around the globe. Our providers elect if they want to partipate on a daily basis, when they have "surplus" capacity and can honor a 24-48 hour turnaround time from the receipt of blanks. The pricing is aggresive as well since it's only applicable when the provider needs / wants to participate. This solves 2 problems that have traditionally plagued the production and fulfillment side of this business: Quick turnaround times and keeping costs affordable, even for low quantity orders.

# Contributors
- Printico, Inc. http://www.printi.co (General Development, All)
- Danijel Bjegovic
